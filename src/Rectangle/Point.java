package Rectangle;

public class Point {
	/** Abscisse du point */

	public int x = 0;

	/** Ordonnée du point */

	public int y = 0;

	/** On peut construire un point en donnant ses coordonnées;
	  @param x abscisse du point
	  @param y ordonnées du point
	 * @return 
	 * @return 
	 */
	// Constructeur 
	public Point(int unX, int unY) {
		x = unX;
		y = unY;
	}
	
	// Par defaut le point construit est l'origine du repére
	public Point() {
		this(0, 0);
	}
	
}
